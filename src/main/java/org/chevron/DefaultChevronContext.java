package org.chevron;

import org.chevron.model.*;

import java.util.*;

public class DefaultChevronContext implements ChevronContext {

    private TemplatesPool templatesPool;
    private ObjectSchemasPool objectSchemasPool;
    private Map<String,Object> definitions = new HashMap<>();
    private Map<String, RegisteredTemplateEngine> enginesMap = new HashMap<>();

    private DefaultChevronContext(){

    }


    @Override
    public Map<String, Object> getDefinitions() {

        return Collections.unmodifiableMap(
                definitions);

    }

    @Override
    public Optional<RegisteredTemplateEngine> getTemplateEngine(String templateLanguage) {

        return Optional.ofNullable(enginesMap.get(
                templateLanguage));

    }

    @Override
    public TemplatesPool getTemplatesPool() {

        return this.templatesPool;

    }

    @Override
    public ObjectSchemasPool getObjectSchemasPool() {

        return this.objectSchemasPool;

    }


    public static Builder builder(){

        return new Builder();

    }

    public static class Builder {

        private DefaultChevronContext context = new DefaultChevronContext();

        public Builder templatesPool(TemplatesPool pool){
            if(pool==null)
                throw new IllegalArgumentException("pool must not be null");
            this.context.templatesPool = pool;
            return this;
        }

        public Builder objectSchemasPool(ObjectSchemasPool pool){
            if(pool==null)
                throw new IllegalArgumentException("pool must not be null");
            this.context.objectSchemasPool = pool;
            return this;
        }


        public <T> Builder register(TemplateEngine<T> engine, T configuration){
            if(engine==null)
                throw new IllegalArgumentException("engine must not be null");
            if(configuration==null)
                throw new IllegalArgumentException("configuration must not be null");
            this.context.enginesMap.put(engine.getLanguage(),new RegisteredTemplateEngineImpl<>(engine,
                    configuration));
            return this;
        }

        public Builder define(String key, Object value){
            if(key==null||key.isEmpty())
                throw new IllegalArgumentException("key must not be null nor empty");
            if(value==null)
                throw new IllegalArgumentException("value must not be null");
            this.context.definitions.put(key,value);
            return this;
        }

        public Builder define(Map<String,Object> definitions){
            if(definitions==null)
                throw new IllegalArgumentException("definitions must not be null");
            this.context.definitions.putAll(definitions);
            return this;
        }

        public DefaultChevronContext build(){
            if(context.objectSchemasPool==null)
                throw new IllegalStateException("an object schemas pool must be set");
            if(context.templatesPool==null)
                throw new IllegalStateException("a templates pool must be set");
            if(context.enginesMap.isEmpty())
                throw new IllegalStateException("there should be at least one (1) registered template engine");
            return this.context;
        }

    }

}
