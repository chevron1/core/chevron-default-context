package org.chevron;

import org.chevron.model.RegisteredTemplateEngine;
import org.chevron.model.TemplateEngine;

public class RegisteredTemplateEngineImpl<ConfigType> implements RegisteredTemplateEngine<ConfigType> {

    private TemplateEngine<ConfigType> engine;
    private ConfigType configType;

    RegisteredTemplateEngineImpl(TemplateEngine<ConfigType> engine, ConfigType configType){
        this.engine = engine;
        this.configType = configType;
    }

    @Override
    public TemplateEngine<ConfigType> getInstance() {
        return this.engine;
    }

    @Override
    public ConfigType getConfiguration() {
        return this.configType;
    }
}
